import Quick
import Nimble

@testable import Weather

final class SHSearchLocationViewModelSpec: QuickSpec {
    
    override func spec() {
        var networkManager: Networkable!
        var weatherProvider: WeatherProviderInterface!
        var viewModel: SHSearchLocationInteractorIO!
        
        beforeEach {
            networkManager = NetworkManager(environment: .local)
            weatherProvider = WeatherProvider(networkManager: networkManager)
            viewModel = SHSearchLocationViewModel(weatherProvider: weatherProvider)
        }
        describe("Searching") {
            it("can fill name city in English") {
                /// Arrange
                let cityName = "Bangkok"
                var count = 0
                
                /// Act
                viewModel.output.locationViewModels.observe {
                    count = $0.count
                }
                viewModel.input.search(with: cityName)
                
                /// Assert
                expect(count).toEventually(equal(1), timeout: 0.5, pollInterval: 0.1)
            }
        }
    }
}
