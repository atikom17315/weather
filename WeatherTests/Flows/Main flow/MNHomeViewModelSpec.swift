import Quick
import Nimble

@testable import Weather

private class MockSettingsManager: SettingsManagerInterface {
    
    private var name: String = "Bangkok"
    private var degree: DegreeType = .celsius
    
    var currentCity: String { return name }
    var currentDegreeType: DegreeType { return degree }
    
    func update(cityName: String) {
        name = cityName
    }
    
    func update(degreeType: DegreeType) {
        degree = (degreeType == .fahrenheit ? .celsius : .fahrenheit)
    }
}

final class MNHomeViewModelSpec: QuickSpec {
    
    override func spec() {
        var settingsManager: SettingsManagerInterface!
        var networkManager: Networkable!
        var weatherProvider: WeatherProviderInterface!
        var viewModel: MNHomeInteractorIO!

        beforeEach {
            settingsManager = MockSettingsManager()
            networkManager = NetworkManager(environment: .local)
            weatherProvider = WeatherProvider(networkManager: networkManager)
            viewModel = MNHomeViewModel(weatherProvider: weatherProvider, settingsManager: settingsManager)
        }
        describe("showing") {
            it("should be shown temperature in correct format for today") {
                /// Arrange
                var temperature = ""

                /// Act
                viewModel.output.temperature.observe {
                    temperature = $0
                }
                viewModel.input.didLoadView()
                
                /// Assert
                expect(temperature).toEventually(equal("33.98\u{f03c}"), timeout: 0.5, pollInterval: 0.1)
            }
            it("should be shown humidity in correct format for today") {
                /// Arrange
                var humidity = ""
                
                /// Act
                viewModel.output.humidity.observe {
                    humidity = $0
                }
                viewModel.input.didLoadView()
                
                /// Assert
                expect(humidity).toEventually(equal("\u{f07a} 53 %"), timeout: 0.5, pollInterval: 0.1)
            }
        }
        
        describe("switching temperature between Celsius and Fahrenheit") {
            context("if current temperature is Celsius and user want to change") {
                it("should be Fahrenheit") {
                    /// Arrange
                    var temperature = ""
                    settingsManager.update(degreeType: .celsius)

                    /// Act
                    viewModel.output.temperature.observe {
                        temperature = $0
                    }
                    viewModel.input.didLoadView()
                    viewModel.input.switchDegree()
                    
                    /// Assert
                    expect(temperature).toEventually(equal("93.164\u{f045}"), timeout: 0.5, pollInterval: 0.1)
                }
            }
            context("if current temperature is Fahrenheit and user want to change") {
                it("should be Celsius") {
                    /// Arrange
                    var temperature = ""
                    settingsManager.update(degreeType: .fahrenheit)
                    
                    /// Act
                    viewModel.output.temperature.observe {
                        temperature = $0
                    }
                    viewModel.input.didLoadView()
                    viewModel.input.switchDegree()
                    
                    /// Assert
                    expect(temperature).toEventually(equal("33.98\u{f03c}"), timeout: 0.5, pollInterval: 0.1)
                }
            }
        }
    }
}
