import Quick
import Nimble

@testable import Weather

private class MockSettingsManager: SettingsManagerInterface {
    
    private var name: String = "Bangkok"
    private var degree: DegreeType = .celsius
    
    var currentCity: String { return name }
    var currentDegreeType: DegreeType { return degree }
    
    func update(cityName: String) {
        name = cityName
    }
    
    func update(degreeType: DegreeType) {
        degree = (degreeType == .fahrenheit ? .celsius : .fahrenheit)
    }
}

final class MNForecastViewModelSpec: QuickSpec {
    
    override func spec() {
        var settingsManager: SettingsManagerInterface!
        var networkManager: Networkable!
        var weatherProvider: WeatherProviderInterface!
        var viewModel: MNForecastViewModel!
        
        beforeEach {
            settingsManager = MockSettingsManager()
            networkManager = NetworkManager(environment: .local)
            weatherProvider = WeatherProvider(networkManager: networkManager)
            viewModel = MNForecastViewModel(weather: nil, weatherProvider: weatherProvider, settingsManager: settingsManager)
        }
        describe("showing") {
            it("should be shown whole-day forecast") {
                /// Arrange
                var count = 0
                var expectCount = 0
                
                async {
                    let forecast = try! await(weatherProvider.getForecast(cityName: "Bangkok"))
                    expectCount = forecast.forecastItems!.reduce(0) {
                        guard let dateTime = $1.dateTime else { return $0 }
                        guard DateInRegion(absoluteDate: dateTime) <= DateInRegion() + 24.hour else { return $0 }
                        return $0 + 1
                    }
                }

                /// Act
                viewModel.output.weatherForecastViewModels.observe {
                    count = $0.count
                }
                viewModel.input.didLoadView()
                
                /// Assert
                expect(count).toEventually(equal(expectCount), timeout: 0.5, pollInterval: 0.1)
            }
        }
    }
}
