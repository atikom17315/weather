extension NSObject {
    
    /// Get a class name from file name.
    /// - Example:
    /// class ABC { // ABC.swift
    ///     init () { }
    /// }
    /// print(ABC().nameOfClass)
    ///
    /// ---------- Out put -----------
    /// ABC
    class var nameOfClass: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}
