extension String {
    
    // MARK: Text
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func trimSpecialCharacters() -> String {
        let special = CharacterSet.init(charactersIn: "/+-() ")
        return self.components(separatedBy: special).joined(separator: "")
    }
}
