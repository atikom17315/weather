extension UICollectionView {
    
    func registerReusableCell<T: UICollectionViewCell>(_: T.Type) where T: Reusable {
        register(T.nib, forCellWithReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(withIndexPath indexPath: IndexPath, execute: (T) -> ()) -> T where T: Reusable {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as?  T else {
            fatalError("Please regiter cell with this \(T.reuseIdentifier)")
        }
        execute(cell)
        cell.setupUI()
        return cell
    }
}
