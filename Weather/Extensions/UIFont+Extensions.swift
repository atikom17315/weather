extension UIFont {
    
    /// Styleguide
    enum FontStyle {
        case extraLight
        case light
        case regular
        case medium
        case bold
    }
    
    class func helveticaNeueFont(ofSize fontSize: CGFloat, fontStyle: FontStyle) -> UIFont {
        let fontName: String
        switch fontStyle {
        case .extraLight: fontName = "HelveticaNeue-ExtraLight"
        case .light: fontName = "HelveticaNeue-Light"
        case .regular: fontName = "HelveticaNeue-Regular"
        case .medium: fontName = "HelveticaNeue-Medium"
        case .bold: fontName = "HelveticaNeue-Bold"
        }
        return UIFont(name: fontName, size: fontSize)!
    }
}
