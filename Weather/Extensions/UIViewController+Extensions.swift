extension UIViewController {
    
    func showToastMsg() -> ((_ message: String) -> Void) {
        return { [weak self] (message) in
            guard let strongSelf = self else { return }
            let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            strongSelf.present(alertController, animated: true, completion: nil)
            
            main(with: 2.0) {
                alertController.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func showAlertMessage() -> ((_ title: String?,
                                _ message: String?,
                                _ cancelButtonTitle: String?,
                                _ moreButtonTitles: String...,
                                _ handler: ((_ index: Int) -> Void)?) -> Void)?
    {
        return { [weak self] (title, message, cancelButtonTitle, moreButtonTitles: String..., handler) in
            guard let strongSelf = self else { return }
            let alertControl = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            if let cancelButtonTitle = cancelButtonTitle {
                alertControl.addAction(UIAlertAction(title: cancelButtonTitle, style: .cancel) { (_) in handler?(0) })
            }
            
            for (idx, val) in moreButtonTitles.enumerated() {
                let index = (cancelButtonTitle == nil ? idx : idx + 1)
                alertControl.addAction(UIAlertAction(title: val, style: .default) { (_) in handler?(index) })
            }
            
            strongSelf.present(alertControl, animated: true, completion: nil)
        }
    }
    
    func showProgressHUD() -> (() -> Void) {
        return { [weak self] in
            guard let _ = self else { return }
            guard let view = UIApplication.shared.keyWindow else { return }
            view.endEditing(true)
            MBProgressHUD.showAdded(to: view, animated: true)
        }
    }
    
    func dismissProgressHUD() -> (() -> Void) {
        return { [weak self] in
            guard let _ = self else { return }
            guard let view = UIApplication.shared.keyWindow else { return }
            MBProgressHUD.hide(for: view, animated: true)
        }
    }
}
