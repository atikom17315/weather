extension Mappable {
    
    static func extractDate(value: Any) -> Date? {
        guard let timeInterval = value as? TimeInterval else { return nil }
        return Date(timeIntervalSince1970: timeInterval)
    }
}
