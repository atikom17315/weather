final class SHSearchHeaderView: SearchBaseView {
    
    // MARK: - Properties
    // MARK: Private
    private var viewModel: SHSearchHeaderInteractorIO!
    private var defaultNavigationBarHeight: CGFloat { return 44.0 }
    private var defaultStatusBarHeight: CGFloat {
        switch SDiOSVersion.deviceSize() {
        case .Screen5Dot8inch: return 44.0 // size of iPhoneX
        default: return 20.0
        }
    }
    
    // MARK: Pubilc & Internal
    var defaultMinimumBarHeight: CGFloat { return 44.0 + defaultNavigationBarHeight + defaultStatusBarHeight }
    var defaultMaximumBarHeight: CGFloat { return 2.0 + defaultMinimumBarHeight }
    
    // MARK: IBOutlet
    @IBOutlet private var navBarView: UIView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var cancelButton: UIButton!
    @IBOutlet var searchBar: UISearchBar!
    
    // MARK: - Methods
    // MARK: Configure
    func configure(_ viewModel: SHSearchHeaderInteractorIO) {
        self.viewModel = viewModel
    }
    
    // MARK: Private
    override func setupUI() {
        setupSearchBar()
        setupAnimation()
        
        /// Set behavior of BLKFlexibleHeightBar library
        let behaviorDefiner = SquareCashStyleBehaviorDefiner()
        behaviorDefiner.addSnappingPositionProgress(0.0, forProgressRangeStart: 0.0, end: 0.5)
        behaviorDefiner.addSnappingPositionProgress(1.0, forProgressRangeStart: 0.5, end: 1.0)
        behaviorDefiner.isSnappingEnabled = true
        behaviorDefiner.isElasticMaximumHeightAtTop = false
        
        self.behaviorDefiner = behaviorDefiner
    }
    
    private func setupSearchBar() {
        searchBar.delegate = self

        // TextField Color Customization
        guard let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField else { return }
        textFieldInsideSearchBar.textColor = .white
        
        // Placeholder Customization
        guard let textFieldInsideSearchBarLabel = textFieldInsideSearchBar.value(forKey: "placeholderLabel") as? UILabel else { return }
        textFieldInsideSearchBarLabel.textColor = .darkGray
    }
    
    private func setupAnimation() {
        let screenWidth = UIScreen.main.bounds.width
        
        minimumBarHeight = defaultMinimumBarHeight
        maximumBarHeight = defaultMaximumBarHeight
        
        /// Setup inital frame
        /// - super view
        frame = CGRect(x: 0.0, y: 0.0, width: screenWidth, height: defaultMaximumBarHeight)
        backgroundColor = .clear
        ///
        /// - titleLabel
        let titleLabelWidth = screenWidth - 32.0
        let titleLabelHeight = CGFloat(18.0)
        let titleLabelX = CGFloat(12.0)
        let titleLabelY =  defaultStatusBarHeight + 16.0
        
        titleLabel.frame = CGRect(x: titleLabelX, y: titleLabelY, width: titleLabelWidth, height: titleLabelHeight)
        ///
        /// - cancelButton
        let cancelButtonWidth = CGFloat(72.0)
        let cancelButtonHeight = CGFloat(56.0)
        let cancelButtonX = screenWidth - cancelButtonWidth
        let cancelButtonY =  titleLabelY + titleLabelHeight - 2
        
        cancelButton.frame = CGRect(x: cancelButtonX, y: cancelButtonY, width: cancelButtonWidth, height: cancelButtonHeight)
        ///
        /// - searchBar
        let searchBarWidth = (screenWidth - cancelButtonWidth) + 8
        let searchBarHeight = CGFloat(56.0)
        let searchBarX = CGFloat(0.0)
        let searchBarY =  titleLabelY + titleLabelHeight - 2
        
        searchBar.frame = CGRect(x: searchBarX, y: searchBarY, width: searchBarWidth, height: searchBarHeight)
        ///
        /// - navBarView
        let navBarViewWidth = screenWidth
        let navBarViewHeight = defaultMaximumBarHeight
        let navBarViewX = CGFloat(0.0)
        let navBarViewY = CGFloat(0.0)
        
        navBarView.frame = CGRect(x: navBarViewX, y: navBarViewY, width: navBarViewWidth, height: navBarViewHeight)
        ///
        /// - blurEffectView
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = navBarView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        navBarView.insertSubview(blurEffectView, at: 0)
        
        /// ===== Animation =====
        ///
        /// Step 1 - navBarView
        let navBarViewAttrStep1 = BLKFlexibleHeightBarSubviewLayoutAttributes()
        navBarViewAttrStep1.alpha = 0.0
        navBarViewAttrStep1.frame = CGRect(x: navBarViewX, y: navBarViewY, width: navBarViewWidth, height: navBarViewHeight)
        navBarView.add(navBarViewAttrStep1, forProgress: 0.0)
        ///
        /// Step 2 - navBarView
        let navBarViewAttrStep2 = BLKFlexibleHeightBarSubviewLayoutAttributes(existing: navBarViewAttrStep1)
        navBarViewAttrStep2?.alpha = 1.0
        navBarViewAttrStep2?.frame = CGRect(x: navBarViewX, y: navBarViewY, width: navBarViewWidth, height: defaultMinimumBarHeight)
        navBarView.add(navBarViewAttrStep2, forProgress: 1.0)
    }
    
    // MARK: IBAction
    @IBAction func cancel(_ sender: Any) {
        viewModel.input.cancelButtonAction()
    }
}

// MARK: - UISearchBarDelegate
extension SHSearchHeaderView: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        viewModel.input.search(with: text)
    }
}
