protocol SHSearchHeaderInteractorIO {
    var input: SHSearchHeaderInteractorInput { get }
    var output: SHSearchHeaderInteractorOutput { get }
}

protocol SHSearchHeaderInteractorInput: class {
    func search(with text: String)
    func cancelButtonAction()
}

protocol SHSearchHeaderInteractorOutput: class {
    /// Properties
    var isCancelled: Observable<Bool> { get }
    
    /// Action
    var search: ((_ text: String) -> Void)? { get set }
}

final class SHSearchHeaderViewModel: SHSearchHeaderInteractorIO {
    
    // MARK: - Properties
    // MARK: InteractorIO
    var input: SHSearchHeaderInteractorInput { return self }
    var output: SHSearchHeaderInteractorOutput { return self }
    
    // MARK: Input
    
    // MARK: Output
    private(set) var isCancelled: Observable<Bool>
    
    var search: ((_ text: String) -> Void)?

    // MARK: - Initialization
    init() {
        self.isCancelled = Observable(false)
    }
}

// MARK: - SHSearchHeaderInteractorInput
extension SHSearchHeaderViewModel: SHSearchHeaderInteractorInput {
    
    func search(with text: String) {
        output.search?(text)
    }
    
    func cancelButtonAction() {
        output.isCancelled.value = true
    }
}

// MARK: - SHSearchHeaderInteractorOutput
extension SHSearchHeaderViewModel: SHSearchHeaderInteractorOutput {
    
}
