protocol SHLocationListInteractorIO {
    var input: SHLocationListInteractorInput { get }
    var output: SHLocationListInteractorOutput { get }
}

protocol SHLocationListInteractorInput: class {
    func cellSelected()
}

protocol SHLocationListInteractorOutput: class {
    /// Properties
    var cityName: Observable<String> { get }
    var time: Observable<String> { get }
    var weatherIcon: Observable<String> { get }
    var temperature: Observable<String> { get }
    var tempMaxMin: Observable<String> { get }
    
    /// Action
    var selectedLocationAt: ((_ weather: WeatherObject) -> Void)? { get set }
}

final class SHLocationListViewModel: SHLocationListInteractorIO {
    
    // MARK: - Properties
    // MARK: Private
    private let settingsManager: SettingsManagerInterface
    private var weather: WeatherObject

    // MARK: InteractorIO
    var input: SHLocationListInteractorInput { return self }
    var output: SHLocationListInteractorOutput { return self }
    
    // MARK: Input
    
    // MARK: Output
    private(set) lazy var cityName: Observable<String> = Observable("")
    private(set) lazy var time: Observable<String> = Observable("")
    private(set) lazy var weatherIcon: Observable<String> = Observable("")
    private(set) lazy var temperature: Observable<String> = Observable("")
    private(set) lazy var tempMaxMin: Observable<String> = Observable("")
    
    var selectedLocationAt: ((_ weather: WeatherObject) -> Void)?
    
    // MARK: - Initialization
    init(weather: WeatherObject, settingsManager: SettingsManagerInterface = SettingsManager.shared) {
        self.weather = weather
        self.settingsManager = settingsManager
        
        guard let weatherItem = weather.weatherItems?.first else { return }
        guard let temperatureItem = weather.temperatureItem,
            let degree = temperatureItem.degree,
            let degreeMin = temperatureItem.degreeMin,
            let degreeMax = temperatureItem.degreeMax else {
            return
        }
        
        let temp: String
        let tempMin: String
        let tempMax: String
        switch settingsManager.currentDegreeType {
        case .celsius:
            temp = "\(degree)\u{f03c}"
            tempMin = "\(degreeMin)°"
            tempMax = "\(degreeMax)°"
        case .fahrenheit:
            temp = "\(Conversions.celsiusToFahrenheit(tempInC: degree))\u{f045}"
            tempMin = "\(Conversions.celsiusToFahrenheit(tempInC: degreeMin))°"
            tempMax = "\(Conversions.celsiusToFahrenheit(tempInC: degreeMax))°"
        }
        
        cityName.value = weather.cityName ?? ""
        time.value = DateInRegion().string(custom: "E, d MMM yyyy HH:mm")
        weatherIcon.value = Conversions.weatherIconToText(condition: weatherItem.id, iconString: weatherItem.iconCode)
        temperature.value = temp
        tempMaxMin.value = "\(tempMax)/\(tempMin)"
    }
}

// MARK: - SHLocationListInteractorInput
extension SHLocationListViewModel: SHLocationListInteractorInput {
    
    func cellSelected() {
        output.selectedLocationAt?(weather)
    }
}

// MARK: - SHLocationListInteractorOutput
extension SHLocationListViewModel: SHLocationListInteractorOutput {
    
}
