final class SHLocationListCell: SearchBaseListCell {
    
    // MARK: - Properties
    // MARK: Private
    private var viewModel: SHLocationListInteractorIO!
    
    // MARK: IBOutlet
    @IBOutlet private var cityNameLabel: UILabel!
    @IBOutlet private var dateTimeLabel: UILabel!
    @IBOutlet private var weatherIconLabel: UILabel!
    @IBOutlet private var temperatureLabel: UILabel!
    @IBOutlet private var tempMaxMinLabel: UILabel!
    
    // MARK: - Methods
    // MARK: Configure
    func configure(_ viewModel: SHLocationListInteractorIO) {
        self.viewModel = viewModel
    }
    
    // MARK: Override
    override func setupUI() {
        super.setupUI()
        bindToViewModel()
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        viewModel.input.cellSelected()
    }
}

// MARK: - Binding
extension SHLocationListCell {
    
    func bindToViewModel() {
        viewModel.output.cityName.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.cityNameLabel.text = $0
        }
        viewModel.output.time.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.dateTimeLabel.text = $0
        }
        viewModel.output.weatherIcon.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.weatherIconLabel.text = $0
        }
        viewModel.output.temperature.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.temperatureLabel.text = $0
        }
        viewModel.output.tempMaxMin.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.tempMaxMinLabel.text = $0
        }
    }
}
