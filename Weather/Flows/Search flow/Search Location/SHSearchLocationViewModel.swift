protocol SHSearchLocationInteractorIO {
    var input: SHSearchLocationInteractorInput { get }
    var output: SHSearchLocationInteractorOutput { get }
}

protocol SHSearchLocationInteractorInput: class {
    func search(with text: String)
}

protocol SHSearchLocationInteractorOutput: class {
    /// Properties
    var searchHeaderViewModel: SHSearchHeaderInteractorIO { get }
    var locationViewModels: Observable<[SHLocationListInteractorIO]> { get }
    
    /// Alert
    var showToastMsg: ((_ msg: String) -> Void)? { get set }
    var showAlertMessage: ((_ title: String?,
                            _ message: String?,
                            _ cancelButtonTitle: String?,
                            _ moreButtonTitles: String...,
                            _ handler: ((_ index: Int) -> Void)?)
                            -> Void)? { get set }
    
    /// Loading
    var showProgressHUD: (() -> Void)? { get set }
    var dismissProgressHUD: (() -> Void)? { get set }
    
    /// Routing
    var backPage: (() -> Void)? { get set }
    
    /// Action
    var selectedLocationAt: ((_ weather: WeatherObject) -> Void)? { get set }
}

final class SHSearchLocationViewModel: SHSearchLocationInteractorIO {
    
    // MARK: - Properties
    // MARK: Provider
    private let weatherProvider: WeatherProviderInterface

    // MARK: InteractorIO
    var input: SHSearchLocationInteractorInput { return self }
    var output: SHSearchLocationInteractorOutput { return self }
    
    // MARK: Input
    
    // MARK: Output
    private(set) lazy var searchHeaderViewModel: SHSearchHeaderInteractorIO = makeViewModel()
    private(set) var locationViewModels: Observable<[SHLocationListInteractorIO]>
    
    var showToastMsg: ((_ msg: String) -> Void)?
    var showAlertMessage: ((_ title: String?,
                            _ message: String?,
                            _ cancelButtonTitle: String?,
                            _ moreButtonTitles: String...,
                            _ handler: ((_ index: Int) -> Void)?)
                            -> Void)?
    
    var showProgressHUD: (() -> Void)?
    var dismissProgressHUD: (() -> Void)?
    
    var backPage: (() -> Void)?
    
    var selectedLocationAt: ((_ weather: WeatherObject) -> Void)?
    
    // MARK: - Initialization
    init(weatherProvider: WeatherProviderInterface = WeatherProvider()) {
        self.weatherProvider = weatherProvider
        self.locationViewModels = Observable([])
    }
    
    // MARK: - Methods
    // MARK: Private
    private func fetchData(cityName: String) {
        locationViewModels.value.removeAll()
        
        async { [weak self] in
            guard let strongSelf = self else { return }
            do {
                let weather = try await(strongSelf.weatherProvider.getWeather(cityName: cityName))
                let viewModel: SHLocationListInteractorIO = strongSelf.makeViewModel(weather: weather)

                main {
                    strongSelf.locationViewModels.value.append(viewModel)
                    strongSelf.output.dismissProgressHUD?()
                }
                
            }  catch let error as ServerHandleError {
                main {
                    strongSelf.handleError(error: error)
                    strongSelf.output.dismissProgressHUD?()
                }
            }
        }
    }
}

// MARK: - SHSearchLocationInteractorInput
extension SHSearchLocationViewModel: SHSearchLocationInteractorInput {
    
    func search(with text: String) {
        fetchData(cityName: text)
    }
}

// MARK: - SHSearchLocationInteractorOutput
extension SHSearchLocationViewModel: SHSearchLocationInteractorOutput {
    
}

// MARK: Helpers
private extension SHSearchLocationViewModel {
    
    func handleError(error: ServerHandleError) {
        switch error {
        case .timeOut, .networkConnectionLost, .notConnectedToInternet:
            output.showToastMsg?(error.localizedDescription)
        default:
            output.showAlertMessage?("Error", error.localizedDescription, nil, "Cancel") { _ in }
        }
    }
    
    private func makeViewModel(weather: WeatherObject) -> SHLocationListInteractorIO {
        let viewModel = SHLocationListViewModel(weather: weather)
        viewModel.output.selectedLocationAt = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.output.selectedLocationAt?($0)
            strongSelf.output.backPage?()
        }
        return viewModel
    }
    
    private func makeViewModel() -> SHSearchHeaderInteractorIO {
        let viewModel = SHSearchHeaderViewModel()
        viewModel.output.search = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.output.showProgressHUD?()
            strongSelf.fetchData(cityName: $0)
        }
        viewModel.output.isCancelled.observe { [weak self] in
            guard let strongSelf = self, $0 else { return }
            strongSelf.output.backPage?()
        }
        return viewModel
    }
}
