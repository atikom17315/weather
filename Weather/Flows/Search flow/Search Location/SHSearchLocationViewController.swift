final class SHSearchLocationViewController: SearchBaseViewController {
    
    // MARK: - Properties
    // MARK: Private
    private var viewModel: SHSearchLocationInteractorIO!
    private var headerView: SHSearchHeaderView!
    private var delegateSplitter: BLKDelegateSplitter!
    
    // MARK: IBOutlet
    @IBOutlet private var tableView: UITableView!
    
    // MARK: - Initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overFullScreen
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        bindToViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        headerView.searchBar.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    // MARK: - Methods
    // MARK: Configure
    func configure(_ viewModel: SHSearchLocationInteractorIO) {
        self.viewModel = viewModel
    }
    
    // MARK: Override
    override func setupUI() {
        super.setupUI()
        setupTableView()
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.insertSubview(blurEffectView, at: 0)
    }
    
    // MARK: Private
    private func setupTableView() {
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        }
        
        tableView.dataSource = self
        tableView.scrollsToTop = true
        tableView.isScrollEnabled = false
        tableView.sectionHeaderHeight = 0.0
        tableView.sectionFooterHeight = 0.0
        tableView.rowHeight = 52.0
        
        tableView.registerReusableCell(SHLocationListCell.self)
        
        /// Set headerView antimation
        headerView = SHSearchHeaderView.loadFromNib() { $0.configure(viewModel.output.searchHeaderViewModel) }
        view.addSubview(headerView)
                
        /// Configure a separate UITableViewDelegate and UIScrollViewDelegate (optional)
        delegateSplitter = BLKDelegateSplitter(firstDelegate: headerView.behaviorDefiner, secondDelegate: self)
        tableView.delegate = delegateSplitter
        tableView.contentInset.top = headerView.defaultMaximumBarHeight
    }
}

// MARK: - Binding
extension SHSearchLocationViewController {
    
    func bindToViewModel() {
        viewModel.output.showToastMsg = showToastMsg()
        viewModel.output.showAlertMessage = showAlertMessage()
        
        viewModel.output.showProgressHUD = showProgressHUD()
        viewModel.output.dismissProgressHUD = dismissProgressHUD()
        
        viewModel.output.backPage = backPage()
        
        viewModel.output.locationViewModels.observe { [weak self] (locationViewModels) in
            guard let strongSelf = self else { return }
            strongSelf.tableView.reloadData()
            strongSelf.tableView.isScrollEnabled = (locationViewModels.count > 0)
        }
    }
    
    func backPage() -> (() -> Void) {
        return { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.dismiss(animated: true, completion: nil)
        }
    }
}

// MARK: - UITableViewDataSource
extension SHSearchLocationViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.output.locationViewModels.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let locationViewModel = viewModel.output.locationViewModels.value[indexPath.row]
        return tableView.dequeueReusableCell(withIndexPath: indexPath) { $0.configure(locationViewModel) } as SHLocationListCell
    }
}
