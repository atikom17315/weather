final class MNWeatherForecastListCell: MainBaseListCell {
    
    // MARK: - Properties
    // MARK: Private
    private var viewModel: MNWeatherForecastListInteractorIO!
    
    // MARK: IBOutlet
    @IBOutlet private var timeLabel: UILabel!
    @IBOutlet private var weatherIconLabel: UILabel!
    @IBOutlet private var tempMaxLabel: UILabel!
    @IBOutlet private var tempMinLabel: UILabel!
    
    // MARK: - Methods
    // MARK: Configure
    func configure(_ viewModel: MNWeatherForecastListInteractorIO) {
        self.viewModel = viewModel
    }
    
    // MARK: Override
    override func setupUI() {
        super.setupUI()
        bindToViewModel()

        backgroundColor = .clear
        contentView.backgroundColor = .clear
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        viewModel.input.cellSelected()
    }
}

// MARK: - Binding
extension MNWeatherForecastListCell {
    
    func bindToViewModel() {
        viewModel.output.time.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.timeLabel.text = $0
        }
        viewModel.output.weatherIcon.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.weatherIconLabel.text = $0
        }
        viewModel.output.temperatureMax.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.tempMaxLabel.text = $0
        }
        viewModel.output.temperatureMin.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.tempMinLabel.text = $0
        }
    }
}
