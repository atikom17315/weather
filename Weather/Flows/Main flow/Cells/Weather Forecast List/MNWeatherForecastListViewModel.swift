protocol MNWeatherForecastListInteractorIO {
    var input: MNWeatherForecastListInteractorInput { get }
    var output: MNWeatherForecastListInteractorOutput { get }
}

protocol MNWeatherForecastListInteractorInput: class {
    func cellSelected()
}

protocol MNWeatherForecastListInteractorOutput: class {
    /// Properties
    var time: Observable<String> { get }
    var weatherIcon: Observable<String> { get }
    var temperatureMax: Observable<String> { get }
    var temperatureMin: Observable<String> { get }
    
    /// Action
    var selectedLocationAt: (() -> Void)? { get set }
}

final class MNWeatherForecastListViewModel: MNWeatherForecastListInteractorIO {
    
    // MARK: - Properties
    // MARK: Private
    private let settingsManager: SettingsManagerInterface
    private let forecastItem: ForecastItemObject

    // MARK: InteractorIO
    var input: MNWeatherForecastListInteractorInput { return self }
    var output: MNWeatherForecastListInteractorOutput { return self }
    
    // MARK: Input
    
    // MARK: Output
    var time: Observable<String> = Observable("")
    var weatherIcon: Observable<String> = Observable("")
    var temperatureMax: Observable<String> = Observable("")
    var temperatureMin: Observable<String> = Observable("")
    
    var selectedLocationAt: (() -> Void)?
    
    // MARK: - Initialization
    init(forecastItem: ForecastItemObject,
         settingsManager: SettingsManagerInterface = SettingsManager.shared) {
        
        self.forecastItem = forecastItem
        self.settingsManager = settingsManager
        
        guard let dateTime = forecastItem.dateTime else { return }
        guard let weatherItem = forecastItem.weatherItems?.first else { return }
        guard let temperatureItem = forecastItem.temperatureItem,
            let degreeMin = temperatureItem.degreeMin,
            let degreeMax = temperatureItem.degreeMax else {
            return
        }
        
        let tempMin: String
        let tempMax: String
        switch settingsManager.currentDegreeType {
        case .celsius:
            tempMin = "\(degreeMin)°"
            tempMax = "\(degreeMax)°"
        case .fahrenheit:
            tempMin = "\(Conversions.celsiusToFahrenheit(tempInC: degreeMin))°"
            tempMax = "\(Conversions.celsiusToFahrenheit(tempInC: degreeMax))°"
        }

        time.value = DateInRegion(absoluteDate: dateTime).string(custom: "HH:mm")
        weatherIcon.value = Conversions.weatherIconToText(condition: weatherItem.id, iconString: weatherItem.iconCode)
        temperatureMax.value = tempMax
        temperatureMin.value = tempMin
    }
}

// MARK: - MNWeatherForecastListInteractorInput
extension MNWeatherForecastListViewModel: MNWeatherForecastListInteractorInput {
    
    func cellSelected() {
        output.selectedLocationAt?()
    }
}

// MARK: - MNWeatherForecastListInteractorOutput
extension MNWeatherForecastListViewModel: MNWeatherForecastListInteractorOutput {
    
}
