final class MNForecastViewController: MainBaseViewController {
    
    // MARK: - Properties
    // MARK: Private
    private var viewModel: MNForecastInteractorIO!
    
    // MARK: IBOutlet
    @IBOutlet private var mainStackView: UIStackView!
    @IBOutlet private var dateTimeLabel: UILabel!
    @IBOutlet private var weatherDescriptionLabel: UILabel!
    @IBOutlet private var temperatureLabel: UILabel!
    
    @IBOutlet private var tableView: UITableView!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        bindToViewModel()
        viewModel.input.didLoadView()
    }
    
    // MARK: - Methods
    // MARK: Configure
    func configure(_ viewModel: MNForecastInteractorIO) {
        self.viewModel = viewModel
    }
    
    // MARK: Override
    override func setupUI() {
        super.setupUI()
        setupTableView()
    }
    
    // MARK: Private
    private func setupTableView() {
        tableView.dataSource = self
        tableView.scrollsToTop = true
        tableView.sectionHeaderHeight = 0.0
        tableView.sectionFooterHeight = 0.0
        tableView.rowHeight = 44.0
        
        tableView.registerReusableCell(MNWeatherForecastListCell.self)
    }
}

// MARK: - Binding
extension MNForecastViewController {
    
    func bindToViewModel() {
        viewModel.output.showToastMsg = showToastMsg()
        viewModel.output.showAlertMessage = showAlertMessage()
        
        viewModel.output.showProgressHUD = showProgressHUD()
        viewModel.output.dismissProgressHUD = dismissProgressHUD()
        
        viewModel.output.title.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.title = $0
        }
        viewModel.output.dateTime.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.dateTimeLabel.text = $0
        }
        viewModel.output.weatherDescription.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.weatherDescriptionLabel.text = $0
        }
        viewModel.output.temperature.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.temperatureLabel.text = $0
        }
        viewModel.output.isHiddenView.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.mainStackView.isHidden = $0
        }
        viewModel.output.weatherForecastViewModels.observe { [weak self] _ in
            guard let strongSelf = self else { return }
            strongSelf.tableView.reloadData()
        }
    }
}

// MARK: - UITableViewDataSource
extension MNForecastViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.output.weatherForecastViewModels.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let weatherForecastViewModel = viewModel.output.weatherForecastViewModels.value[indexPath.row]
        return tableView.dequeueReusableCell(withIndexPath: indexPath) { $0.configure(weatherForecastViewModel) } as MNWeatherForecastListCell
    }
}
