protocol MNForecastInteractorIO {
    var input: MNForecastInteractorInput { get }
    var output: MNForecastInteractorOutput { get }
}

protocol MNForecastInteractorInput: class {
    func didLoadView()
}

protocol MNForecastInteractorOutput: class {
    /// Properties
    var title: Observable<String> { get }
    var dateTime: Observable<String> { get }
    var weatherDescription: Observable<String> { get }
    var temperature: Observable<String> { get }
    var isHiddenView: Observable<Bool> { get }
    var weatherForecastViewModels: Observable<[MNWeatherForecastListInteractorIO]> { get }
    
    /// Alert
    var showToastMsg: ((_ msg: String) -> Void)? { get set }
    var showAlertMessage: ((_ title: String?,
                            _ message: String?,
                            _ cancelButtonTitle: String?,
                            _ moreButtonTitles: String...,
                            _ handler: ((_ index: Int) -> Void)?)
                            -> Void)? { get set }
    
    /// Loading
    var showProgressHUD: (() -> Void)? { get set }
    var dismissProgressHUD: (() -> Void)? { get set }
}

final class MNForecastViewModel: MNForecastInteractorIO {
    
    // MARK: - Properties
    // MARK: Provider
    private let weatherProvider: WeatherProviderInterface

    // MARK: Private
    private let settingsManager: SettingsManagerInterface
    private var weather: WeatherObject?

    // MARK: InteractorIO
    var input: MNForecastInteractorInput { return self }
    var output: MNForecastInteractorOutput { return self }
    
    // MARK: Input
    
    // MARK: Output
    private(set) var title: Observable<String> = Observable("")
    private(set) var dateTime: Observable<String> = Observable("")
    private(set) var weatherDescription: Observable<String> = Observable("")
    private(set) var temperature: Observable<String> = Observable("")
    private(set) var isHiddenView: Observable<Bool> = Observable(true)
    private(set) var weatherForecastViewModels: Observable<[MNWeatherForecastListInteractorIO]> = Observable([])
    
    var showToastMsg: ((_ msg: String) -> Void)?
    var showAlertMessage: ((_ title: String?,
                            _ message: String?,
                            _ cancelButtonTitle: String?,
                            _ moreButtonTitles: String...,
                            _ handler: ((_ index: Int) -> Void)?)
                            -> Void)?
    
    var showProgressHUD: (() -> Void)?
    var dismissProgressHUD: (() -> Void)?
    
    // MARK: - Initialization
    init(weather: WeatherObject?,
        weatherProvider: WeatherProviderInterface = WeatherProvider(),
        settingsManager: SettingsManagerInterface = SettingsManager.shared) {
        
        self.weather = weather
        
        self.weatherProvider = weatherProvider
        self.settingsManager = settingsManager
        
        guard let weather = weather else { return }
        guard let weatherItem = weather.weatherItems?.first else { return }
        guard let temperatureItem = weather.temperatureItem, let degree = temperatureItem.degree else { return }
        
        let temp: String
        switch settingsManager.currentDegreeType {
        case .celsius:
            temp = "\(degree)\u{f03c}"
        case .fahrenheit:
            temp = "\(Conversions.celsiusToFahrenheit(tempInC: degree))\u{f045}"
        }
        
        title.value = settingsManager.currentCity
        dateTime.value = DateInRegion().string(custom: "E, d MMM yyyy HH:mm")
        weatherDescription.value = weatherItem.main ?? "-"
        temperature.value = "\(Conversions.weatherIconToText(condition: weatherItem.id, iconString: weatherItem.iconCode)) \(temp)"
        isHiddenView.value = false
    }
    
    // MARK: - Methods
    // MARK: Private
    private func fetchData(cityName: String) {
        async { [weak self] in
            guard let strongSelf = self else { return }
            do {
                let forecast = try await(strongSelf.weatherProvider.getForecast(cityName: cityName))
                let viewModels: [MNWeatherForecastListInteractorIO]? = forecast.forecastItems?.compactMap {
                    guard let dateTime = $0.dateTime else { return nil }
                    guard DateInRegion(absoluteDate: dateTime) <= DateInRegion() + 24.hour else { return nil }
                    return strongSelf.makeViewModel(forecastItem: $0)
                }
                
                main {
                    strongSelf.weatherForecastViewModels.value = viewModels ?? []
                    strongSelf.output.dismissProgressHUD?()
                }
                
            }  catch let error as ServerHandleError {
                main {
                    strongSelf.handleError(error: error)
                    strongSelf.output.dismissProgressHUD?()
                }
            }
        }
    }
}

// MARK: - MNForecastInteractorInput
extension MNForecastViewModel: MNForecastInteractorInput {
    
    func didLoadView() {
        output.showProgressHUD?()
        
        let cityName = settingsManager.currentCity
        fetchData(cityName: cityName)
    }
}

// MARK: - MNForecastInteractorOutput
extension MNForecastViewModel: MNForecastInteractorOutput {
    
}

// MARK: Helpers
private extension MNForecastViewModel {
    
    func handleError(error: ServerHandleError) {
        switch error {
        case .timeOut, .networkConnectionLost, .notConnectedToInternet:
            output.showToastMsg?(error.localizedDescription)
        default:
            output.showAlertMessage?("Error", error.localizedDescription, nil, "Cancel") { _ in }
        }
    }
    
    private func makeViewModel(forecastItem: ForecastItemObject) -> MNWeatherForecastListInteractorIO {
        let viewModel = MNWeatherForecastListViewModel(forecastItem: forecastItem)
        return viewModel
    }
}
