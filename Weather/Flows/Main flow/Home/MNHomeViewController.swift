final class MNHomeViewController: MainBaseViewController {

    // MARK: - Properties
    // MARK: Private
    private var viewModel: MNHomeInteractorIO!
    
    // MARK: IBOutlet
    @IBOutlet private var mainStackView: UIStackView!
    @IBOutlet private var cityNameLabel: UILabel!
    @IBOutlet private var weatherIconLabel: UILabel!
    @IBOutlet private var temperatureLabel: UILabel!
    
    @IBOutlet private var humidityLabel: UILabel!
    @IBOutlet private var forecastButton: UIButton!
    @IBOutlet private var switchDegreeButton: UIButton!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        bindToViewModel()
        viewModel.input.didLoadView()
    }
    
    // MARK: - Methods
    // MARK: Configure
    func configure(_ viewModel: MNHomeInteractorIO) {
        self.viewModel = viewModel
    }

    // MARK: Routing
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch segue.destination {
            
        case let viewController as MNForecastViewController where segue.identifier == "PushToForecast":
            guard let viewModel = sender as? MNForecastInteractorIO else { return }
            viewController.configure(viewModel)
            
        case let viewController as SHSearchLocationViewController where segue.identifier == "PresentToSearchLocation":
            guard let viewModel = sender as? SHSearchLocationInteractorIO else { return }
            viewController.configure(viewModel)
            
        default: break
        }
    }
    
    // MARK: IBAction
    @IBAction func extendedForecast(_ sender: Any) {
        viewModel.input.extendedForecastAction()
    }
    
    @IBAction private func searchLocation(_ sender: Any) {
        viewModel.input.searchLocationAction()
    }
    
    @IBAction func switchDegree(_ sender: Any) {
        viewModel.input.switchDegree()
    }
}

// MARK: - Binding
extension MNHomeViewController {
    
    func bindToViewModel() {
        viewModel.output.showToastMsg = showToastMsg()
        viewModel.output.showAlertMessage = showAlertMessage()
        
        viewModel.output.showProgressHUD = showProgressHUD()
        viewModel.output.dismissProgressHUD = dismissProgressHUD()
        
        viewModel.output.showForecastPage = showForecastPage()
        viewModel.output.showSearchLocationPage = showSearchLocationPage()
        
        viewModel.output.cityName.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.cityNameLabel.text = $0
        }
        viewModel.output.weatherIcon.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.weatherIconLabel.text = $0
        }
        viewModel.output.temperature.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.temperatureLabel.text = $0
        }
        viewModel.output.humidity.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.humidityLabel.text = $0
        }
        viewModel.output.isHiddenView.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.mainStackView.isHidden = $0
            strongSelf.forecastButton.isHidden = $0
        }
        viewModel.output.degreeSwitch.observe { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.switchDegreeButton.setAttributedTitle($0, for: .normal)
        }
    }
    
    func showForecastPage() -> ((_ viewModel: MNForecastInteractorIO) -> Void) {
        return { [weak self] (viewModel) in
            guard let strongSelf = self else { return }
            strongSelf.performSegue(withIdentifier: "PushToForecast", sender: viewModel)
        }
    }
    
    func showSearchLocationPage() -> ((_ viewModel: SHSearchLocationInteractorIO) -> Void) {
        return { [weak self] (viewModel) in
            guard let strongSelf = self else { return }
            strongSelf.performSegue(withIdentifier: "PresentToSearchLocation", sender: viewModel)
        }
    }
}
