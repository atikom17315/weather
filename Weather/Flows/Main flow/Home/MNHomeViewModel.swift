protocol MNHomeInteractorIO {
    var input: MNHomeInteractorInput { get }
    var output: MNHomeInteractorOutput { get }
}

protocol MNHomeInteractorInput: class {
    func didLoadView()
    func switchDegree()
    func extendedForecastAction()
    func searchLocationAction()
}

protocol MNHomeInteractorOutput: class {
    /// Properties
    var cityName: Observable<String> { get }
    var weatherIcon: Observable<String> { get }
    var temperature: Observable<String> { get }
    var humidity: Observable<String> { get }
    var isHiddenView: Observable<Bool> { get }
    var degreeSwitch: Observable<NSAttributedString> { get }

    /// Alert
    var showToastMsg: ((_ msg: String) -> Void)? { get set }
    var showAlertMessage: ((_ title: String?,
                            _ message: String?,
                            _ cancelButtonTitle: String?,
                            _ moreButtonTitles: String...,
                            _ handler: ((_ index: Int) -> Void)?)
                            -> Void)? { get set }
    
    /// Loading
    var showProgressHUD: (() -> Void)? { get set }
    var dismissProgressHUD: (() -> Void)? { get set }
    
    /// Routing
    var showForecastPage: ((_ viewModel: MNForecastInteractorIO) -> Void)? { get set }
    var showSearchLocationPage: ((_ viewModel: SHSearchLocationInteractorIO) -> Void)? { get set }
}

final class MNHomeViewModel: MNHomeInteractorIO {
    
    // MARK: - Properties
    // MARK: Provider
    private let weatherProvider: WeatherProviderInterface

    // MARK: Private
    private let settingsManager: SettingsManagerInterface
    private var weather: WeatherObject? {
        didSet {
            guard let weather = weather else { return }
            guard let weatherItem = weather.weatherItems?.first else { return }
            guard let temperatureItem = weather.temperatureItem, let degree = temperatureItem.degree else { return }
            
            let temp: String
            switch settingsManager.currentDegreeType {
            case .celsius:
                temp = "\(degree)\u{f03c}"
            case .fahrenheit:
                temp = "\(Conversions.celsiusToFahrenheit(tempInC: degree))\u{f045}"
            }
            
            cityName.value = weather.cityName ?? "-"
            weatherIcon.value = Conversions.weatherIconToText(condition: weatherItem.id, iconString: weatherItem.iconCode)
            temperature.value = temp
            humidity.value = "\u{f07a} \(temperatureItem.percentOfHumidity ?? 0) %"
            isHiddenView.value = false
        }
    }

    // MARK: InteractorIO
    var input: MNHomeInteractorInput { return self }
    var output: MNHomeInteractorOutput { return self }
    
    // MARK: Input
    
    // MARK: Output
    private(set) var cityName: Observable<String> = Observable("")
    private(set) var weatherIcon: Observable<String> = Observable("")
    private(set) var temperature: Observable<String> = Observable("")
    private(set) var humidity: Observable<String> = Observable("")
    private(set) var isHiddenView: Observable<Bool> = Observable(true)
    private(set) var degreeSwitch: Observable<NSAttributedString> = Observable(NSAttributedString(string: "°C / °F"))

    var showToastMsg: ((_ msg: String) -> Void)?
    var showAlertMessage: ((_ title: String?,
                            _ message: String?,
                            _ cancelButtonTitle: String?,
                            _ moreButtonTitles: String...,
                            _ handler: ((_ index: Int) -> Void)?)
                            -> Void)?
    
    var showProgressHUD: (() -> Void)?
    var dismissProgressHUD: (() -> Void)?

    var showForecastPage: ((_ viewModel: MNForecastInteractorIO) -> Void)?
    var showSearchLocationPage: ((_ viewModel: SHSearchLocationInteractorIO) -> Void)?
    
    // MARK: - Initialization
    init(weatherProvider: WeatherProviderInterface = WeatherProvider(),
         settingsManager: SettingsManagerInterface = SettingsManager.shared) {
        
        self.weatherProvider = weatherProvider
        self.settingsManager = settingsManager
        
        degreeSwitch.value = getCurrentAttributedStringOfDegreeSwitch()
    }
    
    // MARK: - Methods
    // MARK: Private
    private func getCurrentAttributedStringOfDegreeSwitch() -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: "°C / °F",
                                                         attributes: [.font: UIFont.helveticaNeueFont(ofSize: 16.0, fontStyle: .light),
                                                                      .foregroundColor: UIColor.lightGray ])
        switch settingsManager.currentDegreeType {
        case .celsius:
            attributedString.addAttribute(.foregroundColor, value: UIColor.white, range: NSRange(location: 0, length: 2))
        case .fahrenheit:
            attributedString.addAttribute(.foregroundColor, value: UIColor.white, range: NSRange(location: 5, length: 2))
        }
        
        return attributedString
    }
    
    private func fetchData(cityName: String) {
        async { [weak self] in
            guard let strongSelf = self else { return }
            do {
                let weather = try await(strongSelf.weatherProvider.getWeather(cityName: cityName))
                
                main {
                    strongSelf.weather = weather
                    strongSelf.output.dismissProgressHUD?()
                    strongSelf.settingsManager.update(cityName: cityName)
                }
                
            }  catch let error as ServerHandleError {
                main {
                    strongSelf.handleError(error: error)
                    strongSelf.output.dismissProgressHUD?()
                }
            }
        }
    }
}

// MARK: - MNHomeInteractorInput
extension MNHomeViewModel: MNHomeInteractorInput {
    
    func didLoadView() {
        output.showProgressHUD?()

        let cityName = settingsManager.currentCity
        fetchData(cityName: cityName)
    }
    
    func switchDegree() {
        switch settingsManager.currentDegreeType {
        case .celsius: settingsManager.update(degreeType: .fahrenheit)
        case .fahrenheit: settingsManager.update(degreeType: .celsius)
        }
        degreeSwitch.value = getCurrentAttributedStringOfDegreeSwitch()
        
        guard let weather = weather else { return }
        guard let temperatureItem = weather.temperatureItem, let degree = temperatureItem.degree else { return }
        
        let temp: String
        switch settingsManager.currentDegreeType {
        case .celsius:
            temp = "\(degree)\u{f03c}"
        case .fahrenheit:
            temp = "\(Conversions.celsiusToFahrenheit(tempInC: degree))\u{f045}"
        }
        
        temperature.value = temp
    }
    
    func extendedForecastAction() {
        let viewModel: MNForecastInteractorIO = makeViewModel(weather: weather)
        showForecastPage?(viewModel)
    }
    
    func searchLocationAction() {
        let viewModel: SHSearchLocationInteractorIO = makeViewModel()
        showSearchLocationPage?(viewModel)
    }
}

// MARK: - MNHomeInteractorOutput
extension MNHomeViewModel: MNHomeInteractorOutput {
    
}

// MARK: Helpers
private extension MNHomeViewModel {
    
    func handleError(error: ServerHandleError) {
        switch error {
        case .timeOut, .networkConnectionLost, .notConnectedToInternet:
            output.showToastMsg?(error.localizedDescription)
        default:
            output.showAlertMessage?("Error", error.localizedDescription, nil, "Cancel") { _ in }
        }
    }
    
    private func makeViewModel(weather: WeatherObject?) -> MNForecastInteractorIO {
        let viewModel = MNForecastViewModel(weather: weather)
        return viewModel
    }
    
    private func makeViewModel() -> SHSearchLocationInteractorIO {
        let viewModel = SHSearchLocationViewModel()
        viewModel.output.selectedLocationAt = { [weak self] in
            guard let strongSelf = self else { return }
            guard let cityName = $0.cityName else { return }
            strongSelf.weather = $0
            strongSelf.settingsManager.update(cityName: cityName)
        }
        return viewModel
    }
}
