class BaseListCell: UITableViewCell, Reusable {

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addObserver()
    }
    
    deinit {
        self.removeObserver()
    }

    // MARK: - Methods
    // MARK: Override
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with:event)
        self.endEditing(true)
    }
    
    // MARK: Pubulic or Internal
    func addObserver() {
        
    }
    
    func removeObserver() {
        
    }
    
    func setupUI() {
        
    }
}
