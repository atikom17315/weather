class BaseViewController: UIViewController {
    
    // MARK: - Properties
    // MARK: Override
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    // MARK: - Initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addObserver()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let navigationController = self.navigationController {
            setupNavigationController(navigationController: navigationController)
        }
    }
    
    deinit {
        removeObserver()
    }
    
    // MARK: - Methods
    // MARK: Override
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with:event)
        view.endEditing(true)
    }
    
    // MARK: Pubulic or Internal
    func addObserver() {
        
    }
    
    func removeObserver() {
        
    }
    
    func setupUI() {
        
    }
    
    @objc func setupLocalization() {
        
    }
    
    func setupNavigationController(navigationController: UINavigationController) {
        /// Set transparent of navigationBar
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController.navigationBar.shadowImage = UIImage()
        navigationController.navigationBar.isTranslucent = true
        navigationController.view.backgroundColor = .clear
        
        /// Set title naviagtionBar
        let attributes = [
            NSAttributedStringKey.font: UIFont.helveticaNeueFont(ofSize: 20.0, fontStyle: .medium),
            NSAttributedStringKey.foregroundColor: UIColor.white
        ]
        navigationController.navigationBar.titleTextAttributes = attributes
        
        /// Set back button
        switch preferredStatusBarStyle {
        case .default, .blackOpaque: navigationController.navigationBar.tintColor = .darkGray
        case .lightContent: navigationController.navigationBar.tintColor = .white
        }
    }
}
