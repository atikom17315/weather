class NetworkManager: Networkable {
    
    // MARK: - Typealias or Enum
    enum Environment {
        case local
        case server
        
        func stubClosure(for target: TargetType) -> Moya.StubBehavior {
            return (self == .server ? .never : .immediate)
        }
    }

    // MARK: - Properties
    // MARK: Private
    private var environment: Environment
    private lazy var plugins: [PluginType] = {
        return [
            NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter),
            NetworkActivityPlugin(networkActivityClosure: { (changeType, t) in
                print(changeType)
                print(t)
            })
        ]
    }()
    
    // MARK: - Methods
    // MARK: Constructor
    
    /// Initialization with setting up environment parameter.
    /// - Parameters:
    ///     - environment: A type of environment it has 2 types.
    ///         1.) server: when you want to call APIs from the server, you have to set it.
    ///         2.) local: if you want to use mock datas from response for testing, you have to set it.
    ///
    init(environment: Environment = .server) {
        self.environment = environment
    }
    
    // MARK: Public or Internal
    
    /// Request APIs and retrue response.
    /// - Example: Login request
    ///
    /// let networkManager = NetworkManager()
    /// networkManager.request(target: AuthAPI.login(username: "appsynth", password: "123456"))
    ///     .then { res in
    ///         print("response: \(res)")
    ///     }.catch let err {
    ///         print("error: \(err)")
    ///     }
    ///
    /// - Parameters:
    ///     - target: A APIs calling, you have to define the target type of Moya
    ///         for API request.
    ///
    func request<Target: TargetType>(target: Target) throws -> Promise<Moya.Response> {
        return async {
            let (moyaResponse, moyaError) = try await(self.call(target: target))
            
            if let response = moyaResponse {
                return response
            } else if let moyaError = moyaError, case let .underlying(nsError as NSError, response) = moyaError {
                throw self.getServerError(error: nsError, response: response)
            } else {
                throw ServerHandleError.unknown
            }
        }
    }
}

// MARK: - Helpers
private extension NetworkManager {
    func call<Target: TargetType>(target: Target) -> Promise<(Moya.Response?, MoyaError?)> {
        return Promise { [weak self] p in
            guard let weakSelf = self else { return }
            let provider = MoyaProvider<Target>(stubClosure: weakSelf.environment.stubClosure, plugins: plugins)
            
            provider.request(target) { result in
                switch result {
                case let .success(response):
                    p.fulfill((response, nil))
                case let .failure(moyaError):
                    p.fulfill((nil, moyaError))
                }
            }
        }
    }
    
    func getServerError(error: NSError, response: Moya.Response?) -> ServerHandleError {
        if let response = response {
            /// HTTP status codes
            switch response.statusCode {
            /// 4xx client error
            case 400: return .badRequest
            case 401: return .unauthorized
            case 403: return .forbidden
            case 404: return .notFound
                
            /// 5xx client error
            case 500: return .serverError
            
            default: return ServerHandleError.unknown
            }
        } else {
            /// NSURL-related Error Codes
            switch error.code {
            case NSURLErrorCancelled: return .cancelled
            case NSURLErrorTimedOut: return .timeOut
            case NSURLErrorNetworkConnectionLost: return .networkConnectionLost
            case NSURLErrorNotConnectedToInternet: return .notConnectedToInternet
                
            default: return ServerHandleError.unknown
            }
        }
    }
    
    func JSONResponseDataFormatter(_ data: Data) -> Data {
        do {
            let dataAsJSON = try JSONSerialization.jsonObject(with: data)
            let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
            return prettyData
        } catch {
            return data // fallback to original data if it can't be serialized.
        }
    }
}
