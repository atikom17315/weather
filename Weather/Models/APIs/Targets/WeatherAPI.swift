enum WeatherAPI {
    case getWeather(cityName: String)
    case getForecast(cityName: String)
}

extension WeatherAPI: TargetType {
    /// The target's base `URL`.
    var baseURL: URL {
        return URL(string: "https://api.openweathermap.org/data/2.5")!
    }
    
    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String {
        switch self {
        case .getWeather: return "/weather"
        case .getForecast: return "/forecast"
        }
    }
    
    /// The HTTP method used in the request.
    var method: Moya.Method {
        switch self {
        case .getWeather: return .get
        case .getForecast: return .get
        }
    }
    
    /// Provides stub data for use in testing.
    var sampleData: Data {
        switch self {
        case .getWeather: return Data(ofResource: "CurrentWeather", withExtension: "json")
        case .getForecast: return Data(ofResource: "WeatherForecast", withExtension: "json")
        }
    }
    
    /// The type of HTTP task to be performed.
    var task: Task {
        let accessTokenOWM = AppConfig.accessTokenOWM ?? ""
        
        switch self {
        case let .getWeather(cityName):
        return .requestParameters(parameters: ["appid": accessTokenOWM, "q": cityName, "units": "metric"], encoding: URLEncoding.default)
        case let .getForecast(cityName):
            return .requestParameters(parameters: ["appid": accessTokenOWM, "q": cityName, "units": "metric"], encoding: URLEncoding.default)
        }
    }
    
    /// The type of validation to perform on the request. Default is `.none`.
    var validationType: ValidationType {
        return .successAndRedirectCodes
    }
    
    /// The headers to be used in the request.
    var headers: [String: String]? {
        var params: [String: String] = [:]
        params["Content-type"] = "application/json"
        return params
    }
}
