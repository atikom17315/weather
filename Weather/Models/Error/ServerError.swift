enum ServerHandleError: Error {
    case unknown
    
    /// HTTP status codes
    /// 4xx client error
    case badRequest                                         // 400 bad request
    case unauthorized                                       // 401 unauthorized
    case forbidden                                          // 403 forbidden
    case notFound                                           // 404 not found
    
    /// 5xx
    case serverError                                        // 500 server error
    
    /// NSURL-related Error Codes
    /// general
    case cancelled                                          // -999 NSURLErrorCancelled
    case timeOut                                            // -1001 NSURLErrorTimedOut
    case networkConnectionLost                              // -1005 NSURLErrorNetworkConnectionLost
    case notConnectedToInternet                             // -1009 NSURLErrorNotConnectedToInternet
    
    var localizedDescription: String {
        switch self {
        case .unknown: return "unknow error"

        /// HTTP status codes
        /// 4xx client error
        case .badRequest: return "400 bad request"
        case .unauthorized: return "401 unauthorized"
        case .forbidden: return "403 forbidden"
        case .notFound: return "404 not found"
            
        /// 5xx
        case .serverError: return "500 server error"
            
        /// NSURL-related Error Codes
        /// general
        case .cancelled: return "Cancelled"
        case .timeOut: return "Timed out"
        case .networkConnectionLost: return "Network Connection Lost"
        case .notConnectedToInternet: return "Not Connected To Internet"
        }
    }
}
