struct ForecastItemObject: Mappable {
    
    let dateTime: Date?
    let weatherItems: [WeatherItemObject]?
    let temperatureItem: TemperatureItemObject?
    
    init(map: Mapper) throws {
        dateTime = map.optionalFrom("dt", transformation: WeatherObject.extractDate)
        weatherItems = map.optionalFrom("weather")
        temperatureItem = map.optionalFrom("main")
    }
}
