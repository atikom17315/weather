struct TemperatureItemObject: Mappable {
    
    let degree: Double?
    let degreeMin: Double?
    let degreeMax: Double?
    let percentOfHumidity: Int?

    init(map: Mapper) throws {
        degree = map.optionalFrom("temp")
        degreeMin = map.optionalFrom("temp_min")
        degreeMax = map.optionalFrom("temp_max")
        percentOfHumidity = map.optionalFrom("humidity")
    }
}

