struct ForecastObject: Mappable {
    
    let city: CityObject?
    let forecastItems: [ForecastItemObject]?

    init(map: Mapper) throws {
        city = map.optionalFrom("city")
        forecastItems = map.optionalFrom("list")
    }
}
