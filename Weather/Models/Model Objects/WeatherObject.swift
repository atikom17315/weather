struct WeatherObject: Mappable {
    
    let cityName: String?
    let dateTime: Date?
    let weatherItems: [WeatherItemObject]?
    let temperatureItem: TemperatureItemObject?
    
    init(map: Mapper) throws {
        cityName = map.optionalFrom("name")
        dateTime = map.optionalFrom("dt", transformation: WeatherObject.extractDate)
        weatherItems = map.optionalFrom("weather")
        temperatureItem = map.optionalFrom("main")
    }
}
