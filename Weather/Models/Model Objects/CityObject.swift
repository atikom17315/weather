struct CityObject: Mappable {
    
    let name: String?
    let country: String?
    
    init(map: Mapper) throws {
        name = map.optionalFrom("name")
        country = map.optionalFrom("country")
    }
}
