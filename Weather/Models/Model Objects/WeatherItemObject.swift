struct WeatherItemObject: Mappable {
    
    let id: Int
    let iconCode: String
    let main: String?
    let description: String?

    init(map: Mapper) throws {
        try id = map.from("id")
        try iconCode = map.from("icon")
        main = map.optionalFrom("main")
        description = map.optionalFrom("description")
    }
}

