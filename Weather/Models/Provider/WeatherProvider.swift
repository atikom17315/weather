protocol WeatherProviderInterface {
    func getWeather(cityName: String) throws -> Promise<WeatherObject>
    func getForecast(cityName: String) throws -> Promise<ForecastObject>
}

final class WeatherProvider: WeatherProviderInterface {
    
    // MARK: - Properties
    // MARK: Private
    private let networkManager: Networkable
    
    // MARK: - Initialization
    init(networkManager: Networkable = NetworkManager()) {
        self.networkManager = networkManager
    }
    
    // MARK: - Methods
    // MARK: Public & Internal
    func getWeather(cityName: String) throws -> Promise<WeatherObject> {
        return async { [weak self] in
            guard let strongSelf = self else { throw ServerHandleError.unknown }
            do {
                let weatherAPI = WeatherAPI.getWeather(cityName: cityName)
                let response = try await(strongSelf.networkManager.request(target: weatherAPI))
                let weather = try response.map(to: WeatherObject.self)
                
                return weather

            } catch let error as ServerHandleError {
                throw error
            }
        }
    }
    
    func getForecast(cityName: String) throws -> Promise<ForecastObject> {
        return async { [weak self] in
            guard let strongSelf = self else { throw ServerHandleError.unknown }
            do {
                let weatherAPI = WeatherAPI.getForecast(cityName: cityName)
                let response = try await(strongSelf.networkManager.request(target: weatherAPI))
                let forecast = try response.map(to: ForecastObject.self)
                
                return forecast
                
            } catch let error as ServerHandleError {
                throw error
            }
        }
    }
    
}
