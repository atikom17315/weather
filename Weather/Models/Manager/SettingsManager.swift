enum DegreeType: Int {
    case celsius
    case fahrenheit
}

protocol SettingsManagerInterface {
    var currentCity: String { get }
    var currentDegreeType: DegreeType { get }
    
    func update(cityName: String)
    func update(degreeType: DegreeType)
}

final class SettingsManager: SettingsManagerInterface {
    
    // MARK: - Properties
    /// MARK: Key of UserDefaults
    private let kCurrentCity = "CityName"
    private let kCurrentDegreeType = "DegreeType"
    
    // MARK: Static
    static let shared: SettingsManagerInterface = {
        return SettingsManager(userDefaults: UserDefaults.standard)
    }()
    
    // MARK: Private
    private let userDefaults: UserDefaults
    
    // MARK: Public & Internal
    var currentCity: String {
        return userDefaults.string(forKey: kCurrentCity) ?? "Bangkok"
    }
    var currentDegreeType: DegreeType {
        guard let degreeType = DegreeType(rawValue: userDefaults.integer(forKey: kCurrentDegreeType)) else { return .celsius }
        return degreeType
    }
    
    // MARK: - Initialization
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }
    
    // MARK: - Methods
    // MARK: Pubilc & Internal
    func update(isFahrenheit: Bool) {
        
    }
    
    func update(cityName: String) {
        userDefaults.set(cityName, forKey: kCurrentCity)
        userDefaults.synchronize()
    }
    
    func update(degreeType: DegreeType) {
        userDefaults.set(degreeType.rawValue, forKey: kCurrentDegreeType)
        userDefaults.synchronize()
    }
}
