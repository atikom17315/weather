protocol Reusable: class {
    static var reuseIdentifier: String { get }
    static var nib: UINib { get }
    
    func setupUI()
    func addObserver()
    func removeObserver()
}

extension Reusable {
    
    static var reuseIdentifier: String {
        return String(describing: Self.self)
    }
    
    /// By default, use the nib which have the same name as the name of the class,
    /// and located in the bundle of that class
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    func setupUI() { }
    func addObserver() { }
    func removeObserver() { }
}


extension Reusable where Self: UIView {
    
    /// Returns a `UIView` object instantiated from nib
    ///     - returns: A `NibLoadable`, `UIView` instance
    static func loadFromNib(execute: ((Self) -> ())) -> Self {
        guard let view = nib.instantiate(withOwner: nil, options: nil).first as? Self else {
            fatalError("The nib \(nib) expected its root view to be of type \(self)")
        }
        execute(view)
        view.setupUI()
        return view
    }
}
