public func main(_ execute: @escaping ()-> ()) {
    DispatchQueue.main.async(execute: execute)
}

public func main(with secondDelay: TimeInterval, execute: @escaping ()-> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + secondDelay, execute: execute)
}
