final class Observable<T> {
    
    // MARK: - Typealias or Enum
    // MARK: Pubilc & Internal
    typealias Observer = (T) -> Void
    
    // MARK: - Properties
    // MARK: Private
    private var observer: Observer?
    
    // MARK: Pubilc & Internal
    var value: T {
        didSet {
            observer?(value)
        }
    }
    
    // MARK: - Initialization
    init(_ value: T) {
        self.value = value
    }
    
    // MARK: - Methods
    // MARK: Pubilc & Internal
    func observe(_ observer: Observer?) {
        self.observer = observer
        observer?(value)
    }
}
