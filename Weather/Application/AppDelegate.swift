@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: - Properties
    // MARK: Pubilc & Internal
    var window: UIWindow?
    var rootController: UINavigationController {
        guard let navigationController = window?.rootViewController as? UINavigationController else {
            fatalError("Please check your rootViewController, it should be UINavigationController")
        }
        return navigationController
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let viewModel = MNHomeViewModel()
        let viewController: MNHomeViewController = UIStoryboard(storyboard: .main).instance()
        
        viewController.configure(viewModel)
        rootController.setViewControllers([viewController], animated: false)
        return true
    }
}

