final class AppConfig {
    
    // MARK: - Properties
    // MARK: Public & Internal
    class var accessTokenOWM: String? {
        return forKey(key: "AccessTokenOWM")
    }
    
    // MARK: - Methods
    // MARK: Private
    private class func forKey(key: String) -> String? {
        return (Bundle.main.infoDictionary?[key] as? String)?.replacingOccurrences(of: "\\", with: "")
    }
}

