# Mobile Application Test - Weather (MVVM: A non-reactive)
Weather is an iOS weather app developed in Swift 4 for testing.

## How to build and test
1) Install pods
```bash
$ cd Weather
$ pod install
```
2) Open the workspace in Xcode
```bash
$ open "Weather.xcworkspace"`
```
3) Sign up on openweathermap.org/appid to get an appid
```bash
$ mkdir .access_tokens
$ echo "your-openweathermap-appid" > .access_tokens/openweathermap
```
4) After you launch app, you will see Bangkok as default city. On this screen, you will see the current weather with temperature and humidity and you can search to see the weather with city name by clicking on search button at bottom-right of screen. If you want to switch temperature between celcius or farenheit, you can click on bottom-left button. To show whole day forecast, please click on text FORECAST you will see 24 hours forecast from now.

## Make brief recommendation for future
- Show current location as default by getting current location from gps
- Able to add city as list to show more than one city's weather and change background image by selected city.
- Able searching with another keyword as city id, geographic coordinates and zipcode etc. (current API provides this also)
- Set forecast duration dynamically by input time period from-to.
- See the weather with map by searching place name or select location on map.

## Requirements
* Xcode 9.4
* iOS 10+
* Swift 4.1